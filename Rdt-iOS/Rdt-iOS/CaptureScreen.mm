//
//  Copyright (C) 2019 University of Washington Ubicomp Lab
//  All rights reserved.
//
//  This software may be modified and distributed under the terms
//  of a BSD-style license that can be found in the LICENSE file.
//

#include "CaptureScreen.h"
#include "ImageQualityViewController.h"
#include "ShowPhotoViewController.h"

@implementation CaptureScreen

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:(@"ReaderEmbedSegue")]) {
        ImageQualityViewController *vc = [segue destinationViewController];
        vc.onRDTDetected = ^(bool passed, UIImage * testStripImage, UIImage * croppedTestStripImage, UIImage * resultWindowImage, bool fiducial, ExposureResult exposureResult, SizeResult sizeResult, bool center, bool orientation, float angle, bool sharpness, bool shadow, bool control, bool testA, bool testB, double captureTime) {
            
            if (!passed) {
                return;
            }
            // This calls the the storyboard
            UIStoryboard *storyb = self.storyboard;
            // This instatnisates the view controller
            ShowPhotoViewController *showPhotoVC = [storyb instantiateViewControllerWithIdentifier:@"photo"];
            
            // this passees the imgage into the showPhotoViewController
            [showPhotoVC setImage: testStripImage];
            [showPhotoVC setResultImage: resultWindowImage];
            [showPhotoVC setControl: control? @"True" : @"False"];
            [showPhotoVC setTestA: testA? @"True" : @"False"];
            [showPhotoVC setTestB: testB? @"True" : @"False"];
            [showPhotoVC setTimeText:[NSString stringWithFormat:@"%.2f seconds", captureTime]];
            
            // This presents the view controller
            [self presentViewController: showPhotoVC animated:YES completion:nil];

        };
    }
}

@end
